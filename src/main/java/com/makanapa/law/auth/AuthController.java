package com.makanapa.law.auth;

import com.makanapa.law.pengguna.PenggunaModel;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(path = "/auth")
@AllArgsConstructor
public class AuthController {

    private static final Logger logger = LogManager.getLogger(AuthController.class);

    @Autowired
    private AuthService authService;

    @PostMapping(path = "/register/pengguna")
    public ResponseEntity<Object> registerPengguna(@RequestBody PenggunaModel penggunaModel) throws IllegalAccessException {
        try {
            PenggunaModel penggunaModel1 = authService.registerPengguna(penggunaModel);
            logger.info("Register Successfully");
            return ResponseEntity.ok(penggunaModel1);
        } catch (Exception e) {
            logger.error("Register Failed");
            return null;
        }
    }
}
