package com.makanapa.law.auth;

import com.makanapa.law.pengguna.PenggunaModel;
import com.makanapa.law.pengguna.PenggunaRepository;
import com.makanapa.law.user.UserModel;
import com.makanapa.law.user.UserModelRepository;
import com.makanapa.law.user.UserModelRole;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lombok.Setter;

@Service
@Setter
public class AuthService {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserModelRepository userModelRepository;

    @Autowired
    public AuthService(UserModelRepository userModelRepository, PasswordEncoder passwordEncoder) {
        this.userModelRepository = userModelRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    private PenggunaRepository penggunaRepository;

    public PenggunaModel registerPengguna(PenggunaModel penggunaModel) throws IllegalAccessException {
        var pengguna = new PenggunaModel();
        pengguna.setNamaLengkap(penggunaModel.getNamaLengkap());
        pengguna.setUsername(penggunaModel.getUsername());
        pengguna.setPassword(penggunaModel.getPassword());
        pengguna.setIdLine(penggunaModel.getIdLine());
        pengguna.setUserRole(UserModelRole.PENGGUNA);

        registrationHelper(pengguna, userModelRepository);
        penggunaRepository.save(pengguna);
        return pengguna;
    }

    public void registrationHelper(UserModel userModel, UserModelRepository repository) throws IllegalAccessException {
        boolean userExist = repository.findByUsername(userModel.getUsername()).isPresent();

        if (userExist) {
            throw new IllegalAccessException("username already taken");
        }

        String encodedPassword = passwordEncoder.encode(userModel.getPassword());
        userModel.setPassword(encodedPassword);
    }
}
