package com.makanapa.law.security.config;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import static com.makanapa.law.security.utils.SecurityConstant.EXPIRATION_TIME;
import static com.makanapa.law.security.utils.SecurityConstant.SECRET;

import com.makanapa.law.security.utils.JWTAuthResponse;
import com.makanapa.law.security.utils.UsernamePasswordAuthRequest;
import com.makanapa.law.user.UserModel;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import lombok.SneakyThrows;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter{
    
    private final AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        setFilterProcessesUrl("/auth");
    }

    @SneakyThrows
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException {
        UsernamePasswordAuthRequest creds = new ObjectMapper().readValue(
                req.getInputStream(),
                UsernamePasswordAuthRequest.class
        );
        return authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        creds.getUsername(),
                        creds.getPassword()
                )
        );
    }

    @Override
    public void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException {
        var calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis()+EXPIRATION_TIME);

        String token;
        if (((UserModel) auth.getPrincipal()).getUserRole().name().equals("PENGGUNA")) {
            token = JWT.create().withSubject(((UserModel) auth.getPrincipal()).getUsername()).withClaim("role",((UserModel) auth.getPrincipal()).getUserRole().name()).withExpiresAt(calendar.getTime()).sign(Algorithm.HMAC512(SECRET.getBytes()));
        } else {
            token = JWT.create().withSubject(((UserModel) auth.getPrincipal()).getUsername()).withClaim("role",((UserModel) auth.getPrincipal()).getUserRole().name()).withExpiresAt(calendar.getTime()).sign(Algorithm.HMAC512(SECRET.getBytes()));
        }

        var jwtAuthResponse = new JWTAuthResponse(token);

        res.addHeader("Access-Control-Expose-Headers", "*");
        res.setContentType("application/json");
        res.setCharacterEncoding("UTF-8");
        res.getWriter().write(jwtAuthResponse.toString());
        res.getWriter().flush();
    }
}
