package com.makanapa.law.pengguna;

import com.makanapa.law.auth.AuthController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

import java.util.Optional;

@RestController
@RequestMapping("/pengguna")
@AllArgsConstructor
public class PenggunaController {

    private static final Logger logger = LogManager.getLogger(AuthController.class);

    @Autowired
    private PenggunaService penggunaService;

    @GetMapping(path = "/detail", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getPenggunaById(@RequestHeader("Authorization") String token) {
        Optional<PenggunaModel> penggunaModel = penggunaService.getPenggunaByUsername(token);
        if (penggunaModel.isEmpty()) {
            logger.info("Pengguna with id " + penggunaModel.get().getId() + " not found.");
            return null;
        } else {
            logger.info("Pengguna not found" );
            return ResponseEntity.ok(penggunaService.getPenggunaByUsername(token));
        }
    }
}
