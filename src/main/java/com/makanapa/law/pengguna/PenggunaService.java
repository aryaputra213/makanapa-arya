package com.makanapa.law.pengguna;

import java.util.Optional;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.makanapa.law.security.utils.SecurityConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PenggunaService {
    
    @Autowired
    private PenggunaRepository penggunaRepository;

    public DecodedJWT decodeJwt(String token) {
        return JWT
                .require(Algorithm.HMAC512(SecurityConstant.SECRET.getBytes()))
                .build()
                .verify(token.replace(SecurityConstant.TOKEN_PREFIX, ""));
    }
    
    public Optional<PenggunaModel> getPenggunaByUsername(String token) {
        DecodedJWT verifier = decodeJwt(token);
        return penggunaRepository.findByUsername(verifier.getSubject());
    }

    public PenggunaModel addNewPengguna(PenggunaModel penggunaModel) {
        return penggunaRepository.save(penggunaModel);
    }
}
