package com.makanapa.law.pengguna;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PenggunaRepository extends JpaRepository<PenggunaModel, Long> {
    public Optional<PenggunaModel> findByUsername(String username);
}
