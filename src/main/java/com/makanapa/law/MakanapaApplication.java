package com.makanapa.law;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MakanapaApplication {
	private static final Logger logger = LogManager.getLogger(MakanapaApplication.class.getName());

	public static void main(String[] args) {
		SpringApplication.run(MakanapaApplication.class, args);
	}

}